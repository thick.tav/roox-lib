//
//  RooxApiTests.swift
//  RooxSDKTests
//
//  Created by Alexandr Timoshenko on 2/27/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation
import XCTest
@testable import RooxSDK

class RooxApiTests: XCTestCase {
    
    
    override func setUp() {
        
    }
    
    func api(headers:[String:String], network:NetworkLayerProtocol)->RooxApiProtocol{
        let config = ConfigBuilder()
            .clientId(_id: "eshop")
            .clientSecret(_secret: "password")
            .url(_url: "https://sso-mgf.demo.rooxteam.com")
            .headers(_headers: headers)
            .build()
        return RooxApi(config: config, network: network)
    }
    
    func testAutoLoginSendsHeaders(){
        let promise = expectation(description: "Api call has headers")
        let network = FakeNetworkLayer(header:[
            SimpleValueValidator(values: ["X-Nokia-MSISDN" : "9876543210","X-Forwarded-For":"5.134.221.198"])
            ],form:[],request:[
            SimpleRequestHandler(calls:["/sso/auth/autologin":ApiResponses.API_RESPONSE_JWT_OK,"/sso/oauth2/access_token":ApiResponses.API_RESPONSE_TOKEN_OK])])
        api(headers: ["X-Nokia-MSISDN":"9876543210","X-Forwarded-For":"5.134.221.198"],network: network).autologin(success: { (response:AutoLoginResponse) in
                promise.fulfill()
        }) { (error:Error) in
            XCTFail("Headers failed")
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testAutoLoginHandleFailure(){
        let promise = expectation(description: "Api call has headers")
        let network = FakeNetworkLayer(header:[],form:[],request:[
            SimpleRequestHandler(calls:[:])])
        api(headers: ["X-Nokia-MSISDN":"9876543210","X-Forwarded-For":"5.134.221.198"],network: network).autologin(success: { (response:AutoLoginResponse) in
            XCTFail("Autologin failed")
        }) { (error:Error) in
            promise.fulfill()
        
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testGetAccessTokenNotSendsHeaders(){
        let promise = expectation(description: "Api call get Access token")
        let network = FakeNetworkLayer(header:[
            SimpleValueValidator(values: ["X-Nokia-MSISDN" : "","X-Forwarded-For":""])
            ],form:[],request:[
                SimpleRequestHandler(calls:["/sso/oauth2/access_token":ApiResponses.API_RESPONSE_TOKEN_OK])])
        api(headers: ["X-Nokia-MSISDN":"9876543210","X-Forwarded-For":"5.134.221.198"],network: network).getAccessToken(formData: [:], success: { (token:AccessTokenResponse) in
            promise.fulfill()
        }) { (error:Error) in
            XCTFail("Get AccessToken failed")
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testGetAccessTokenHandlesAuthError(){
        let promise = expectation(description: "Api call get Access token")
        let network = FakeNetworkLayer(header:[
            SimpleValueValidator(values: [:])
            ],form:[],request:[
                SimpleRequestHandler(calls:["/sso/oauth2/access_token":ApiResponses.API_RESPONSE_USERNAME_PASS_REQUIRED])])
        api(headers: [:],network: network).getAccessToken(formData: [:], success: { (token:AccessTokenResponse) in
            XCTFail("Get AccessToken failed")
        }) { (error:Error) in
            if (error is AuthenticateError){
                promise.fulfill()
            }else{
                XCTFail("Get AccessToken failed")
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    func testGetAccessTokenHandlesGeneralError(){
        let promise = expectation(description: "Api call get Access token")
        let network = FakeNetworkLayer(header:[
            SimpleValueValidator(values: [:])
            ],form:[],request:[
                SimpleRequestHandler(calls:[:])])
        api(headers: [:],network: network).getAccessToken(formData: [:], success: { (token:AccessTokenResponse) in
            XCTFail("Get AccessToken failed")
        }) { (error:Error) in
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    override func tearDown() {
        
    }

}
