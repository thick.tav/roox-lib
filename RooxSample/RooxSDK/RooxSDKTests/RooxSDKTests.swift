//
//  RooxSDKTests.swift
//  RooxSDKTests
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import XCTest
@testable import RooxSDK

class RooxSDKTests: XCTestCase {

    var sdk:UIDM?
    override func setUp() {
        
    }
    
    func getSdk(headers:[String:String])->UIDM{
        sdk = UIDM.create(config: ConfigBuilder()
            .clientId(_id: "eshop")
            .clientSecret(_secret: "password")
            .url(_url: "https://sso-mgf.demo.rooxteam.com")
            .headers(_headers: headers)
            .build())
        return sdk!
    }

    override func tearDown() {
        sdk?.store().remove()
    }

    func testShouldReturnNoTokenError() {
        let promise = expectation(description: "Error is NoToken")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = EmptyNetwork()
        s.getToken(success: { (Token) in
             XCTFail("Got token without user")
        }, failure: { (error:Error) in
            if (error is NoTokenError){
                promise.fulfill()
            }else{
                XCTFail("Not NoToken Error")
            }
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testShouldAutologin(){
        let promise = expectation(description: "Autologin failed")
        let s = getSdk(headers: ["X-Nokia-MSISDN":"9876543210","X-Forwarded-For":"5.134.221.198"])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinOkRequest])
        s.sso().authenticate(success: { (token:Token) in
            promise.fulfill()
        }, failure: { (error:Error) in
             XCTFail("Token error")
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testAutologinShouldreturnSignInForm(){
        let promise = expectation(description: "Autologin error failed")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinUserNameRequest])
        s.sso().authenticate(success: { (token:Token) in
            XCTFail("Got autologin success")
        }, failure: { (error:Error) in
            if (error is AuthenticateError){
                promise.fulfill()
            }else{
                XCTFail("Error not AuthenticateError")
            }
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testUserNameLoginShouldSuccess(){
        let promise = expectation(description: "UserPass login ok")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinUserNameRequest])
        s.sso().authenticate(success: { (Token) in
            XCTFail("Got token with autologin")
        }) { (error:Error) in
            if (error is AuthenticateError){
                (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [SimpleValueValidator(values: ["username":"9876543210","password":"password"])], request: [Samples.GetTokenUserNameRequestOK])
                self.sdk?.sso().continueAuthenticate(formData: ["username" : "9876543210","password":"password"], success: { (toke:Token) in
                    promise.fulfill()
                }, failure: { (error:Error) in
                    XCTFail("Failed to login")
                })
            }else{
                XCTFail("Error not AuthenticateError")
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testUserNameWrongPasswordShouldReturnError(){
        let promise = expectation(description: "UserPass login error")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinUserNameRequest])
        s.sso().authenticate(success: { (Token) in
            XCTFail("Got token with autologin")
        }) { (error:Error) in
            if (error is AuthenticateError){
                (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.GetTokenUserNameRequestWrong])
                self.sdk?.sso().continueAuthenticate(formData: ["username" : "9876543210","password":"wrong password"], success: { (toke:Token) in
                    XCTFail("Login was ok")
                }, failure: { (error:Error) in
                    if (error is AuthenticateError){
                        promise.fulfill()
                    }else{
                        XCTFail("Error not AuthenticateError")
                    }
                })
            }else{
                XCTFail("Error not AuthenticateError")
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testUserOTPLoginShouldAutologinError(){
        let promise = expectation(description: "Autologin error failed")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinOTPRequest])
        s.otp().authenticate(success: { (token:Token) in
            XCTFail("Got autologin success")
        }, failure: { (error:Error) in
            if (error is AuthenticateError){
                promise.fulfill()
            }else{
                XCTFail("Error not AuthenticateError")
            }
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    func testUserOTPLoginShouldReturnOtpForm(){
        let promise = expectation(description: "Autologin error failed")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinOTPForm])
        s.otp().authenticate(success: { (token:Token) in
            XCTFail("Got autologin success")
        }, failure: { (error:Error) in
            if (error is AuthenticateError){
                if ((error as! AuthenticateError).authForm.step == "login-by-otp-form"){
                    promise.fulfill()
                }else{
                    XCTFail("Not otp form response")
                }
                
            }else{
                XCTFail("Error not AuthenticateError")
            }
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    func testUserOTPLoginShouldSendMSIDNAndRespond(){
        let promise = expectation(description: "Autologin error failed")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinOTPForm])
        s.otp().authenticate(success: { (token:Token) in
            XCTFail("Got autologin success")
        }, failure: { (error:Error) in
            if (error is AuthenticateError){
                if ((error as! AuthenticateError).authForm.step == "login-by-otp-form"){
                    (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [SimpleValueValidator(values: ["msisdn":"9876543210"])], request: [Samples.AutologinOTPRequest])
                    s.otp().continueAuthenticate(formData: ["msisdn":"9876543210"], success: { (token:Token) in
                        XCTFail("Got autologin success")
                    }, failure: { (error:Error) in
                        if (error is AuthenticateError){
                            if ((error as! AuthenticateError).authForm.step == "enter_otp_form"){
                                promise.fulfill()
                            }else{
                                XCTFail("Not otp form response")
                            }
                        }else{
                            XCTFail("Error not AuthenticateError")
                        }
                    })
                }else{
                    XCTFail("Not otp form response")
                }
                
            }else{
                XCTFail("Error not AuthenticateError")
            }
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    func testUserOTPLoginShouldSendOtpCodeAndRespond(){
        let promise = expectation(description: "Autologin error failed")
        let s = getSdk(headers: [:])
        (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [], request: [Samples.AutologinOTPForm])
        s.otp().authenticate(success: { (token:Token) in
            XCTFail("Got autologin success")
        }, failure: { (error:Error) in
            if (error is AuthenticateError){
                if ((error as! AuthenticateError).authForm.step == "login-by-otp-form"){
                    (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [SimpleValueValidator(values: ["msisdn":"9876543210"])], request: [Samples.AutologinOTPRequest])
                    s.otp().continueAuthenticate(formData: ["msisdn":"9876543210"], success: { (token:Token) in
                        XCTFail("Got autologin success")
                    }, failure: { (error:Error) in
                        if (error is AuthenticateError){
                            if ((error as! AuthenticateError).authForm.step == "enter_otp_form"){
                                (s._api as! RooxApi)._network = FakeNetworkLayer(header: [], form: [SimpleValueValidator(values: ["otpCode":"1234"])], request: [Samples.TokenGetOkRequest])
                                s.otp().continueAuthenticate(formData: ["otpCode":"1234"], success: { (Token) in
                                    promise.fulfill()
                                }, failure: { (Error) in
                                    XCTFail("Failed to otp login")
                                })
                            }else{
                                XCTFail("Not otp form response")
                            }
                        }else{
                            XCTFail("Error not AuthenticateError")
                        }
                    })
                }else{
                    XCTFail("Not otp form response")
                }
                
            }else{
                XCTFail("Error not AuthenticateError")
            }
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
