//
//  ApiResponses.swift
//  RooxSDKTests
//
//  Created by Alexandr Timoshenko on 2/27/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation
struct ApiResponses {
    static let API_RESPONSE_JWT_OK = "{\"auto-login-jwt\":\"jwtToken\"}"
    static let API_RESPONSE_TOKEN_OK =
    "{\"access_token\":\"7a0a6d96-d13f-481c-9b9f-cc3850563d2d\",\"mpt\":\"526062c1-1de3-4dca-9d8a-f666899633e2\",\"refresh_token\":\"b2e38e3b-f640-4dea-a7ea-5d9bac1d3ca8\",\"scope\":[\"cn\",\"impersonator\"],\"token_type\":\"Bearer\",\"expires_in\":59,\"JWTToken\":\"jwtToken\"}"
    static let API_RESPONSE_USERNAME_PASS_REQUIRED = "{\n" +
    "    \"execution\": \"89cfbcb4-24b1-459b-ae70-b3b0fbc36a18_AAABFQAAAQDsrHcE7UuZMuKrtdTSOFaTbuylTUKwBHcwCUPq/sOc4rGB0kZtOhxQvBZIY66sYlyK3geLy9omIzFbQKMdJ3dhD58ONNMd43B/YW/xAf6gAxlhL0KqBZic9lh9LX938RqmPluqGwFbjM/9DWPfYzvwpQCwfL8wa1c3jLE1D466CJDMSYWrZr/hszqEkMu5ofGfgYr8i+j0mYpReyD7P2rR/whdctnY1mWjMGTADrh0GSLVvRRfjcpzD+zryGycu9EJbAXKhPlpNLyVpXS9QlPGqCXrA69BZntU1+WCgyS0ufjivPAw3XtBcp0mFDN3XeFaKHiNVsNzrNlgBqXzFgK1AAAACWV4ZWN1dGlvbvpJhSV0WN1rQeyKywOv3eD35RWwOwmtPymhHFJKWY3XDPNPVURyrtFr3fVcGPnrwMWoQs9/I8G5eBYFfsKEH27+tO0n4G0olT5OkihU3ysxKij9CSXqDkKC+pZptWPWCybJQmc/PAmEFCiB4LC/u121kjis3GWRS0KGxZYQ0aanwVvqy9VeiIg86WS3CuVX8pl5FjN9DXOC5ltNbWRLV06Eg8tl2THMMrNeovuiaoOFv06kHgdrSZRsFf+Q0F0JIn/pT7UhkmRGHE95jCYt/0OhPIPTlyrFVRMjIzI5ihhgaVNHdpDaGs0J6NUsvXNJGw6drh00Jqpc04bKJBeYJ1lbIpjhZRHHbFGPJ+GnDTN5aggDdIFRw1g9ZgAtukeEIrdvlTUsOJh14u3QV5U+vZJLlS1S5VTG9T5jH96OemilET7zH7fM+tkTgIhObygidqIMvnFbt89tjpNa6umvXBH+TvD3xBNRgHGYqZO1hWJi0RH8Zti40QXQbbVc8gqiisMYS5Ays2wX53WNQeYs3MrlI6b84HH1L4wbK1s3zTlT+87pqXSL5JSoP/0R23+s4RW+XEBW3s3clNMg/Te+8XjikwCYagEXOZ3/p6BxC/+EMAVsDr0w11V+RdhE/XfF2cWcDerMbe78YJgoDeZ9eZ341G0snV2AYJ7xI0gk+G0rWSI2QsSLvJB06xIaT0cwywhYhM2lxbQ+28BPAti1wiNDXhG4iw6BH7wqF9fYtkR81TWs1RK7XQoPUSBel6C4IWRcehWEfBUHG4IDBDlRcOZrbJvHrUenhu+n6aAUkt/n0pM0Lychsct1fnsIlS8omHCMRROf6yW0VaC1XXDEnHPknqnr09z9CjX2ZDC/i9qMZT4FwaNm0r/nJugTVuqPmSCaoee50tYxAxoFUZIXS40K6u8nI62yu9Jgj62UnKeqqyzC2Rzj7suhtJkcgbRxCWOP5tSxWW3DcELNPwDjLUtK7Dz+RJH9tAT9K42nDtrQBkK/TOXK658QOuQCPgaifnkFjfcZxtuYln2YrNojmysfeyCk0mUFXkVENRkepEsxFCMp+OGvigsyaUqJYkcYXJi9IgaYxzPGMz/D49NJZtf4kQVzf80jqTbh2fS4hHSrv83hUMRPVYngpJR/+NRXO9SO2Yka/qzCNrLUgGyHepOvPf/apgI3HlW2GQQsznjvsIAfC1lY8CxjpNoh9MvByH07Oyrbq5dozTdqc3nvsrtCriUvoG0LylFxwbSOhBne84pA0Jyd0SqjesdDnC9Q6E60VNahoQGh/v5jKS4oelAi6uEedAhQWdA7YkbfR9IH8Rx8XWZpgThJBGXRAj8VeKGKYJoBC5KUsksBFUyOzp9xh596QHJOIX2kbTy4zGz0mljF3P7MnMm5Pu8eIFhgaHgItK4bzMA1mYjp/hASCbw09dJFJQqOhc24m29M4IqwnHxCA8/4IkmxD4htpLG+AYfoXpaIQEj/sKejbCJXvb9K2doY8Zveaw65/+pWBbuuF/IMBntOc3C7yzL8b3F0Db2xKT/rKNDBtbAIvtIKn3TYeu4cxVOjJBVJr0IO0/CS9c+gYL1HbWzKzZIEQmZtSM4vBimbS3rmWmwWl6ex0nzv6nxghfYXxDqRWdgR+w0iccdn0McKjJ5TnhEPhCL5NLkimOlK9ijFPER7wTbFcS31vSYt5xG6QeC1pgkLxboY1evoB+2BoZmnemFTfi+U9qMKayUhsjjZTmF7C5M449vUnUb2tAluqKe03PIpBjhGftKMjadlby9vSF/xqR10XeRkRVNXkhZtfkPAFTzSZrpCmNFDv35xCDPJK8nzbo6vKGhorVMg5OYNy+N0EDksVSZaTbIZV6SRJVjcZMBnam6zPCMfVA58nynyehQ16FP79UrciqQ217G9IT3xV8DrhbhxaO79ozaZEJtf9m87tlQI/RR5MxG8NoQHBx0yo9JAowM6x58pgjZh5AKEJ5UKFlh7xb3KoTwxrYDWVAwKHi5lETYlxos9I8g4NWD43WfZp6t1kQxe1pkgQY980e95t1TAv58=\",\n" +
    "    \"view\": {\n" +
    "        \"facebookAppId\": \"161417944516938\",\n" +
    "        \"mailRuRequestScopesAsArray\": [],\n" +
    "        \"googleRequestScopesAsArray\": [\n" +
    "            \"https://www.googleapis.com/auth/userinfo.profile\",\n" +
    "            \"https://www.googleapis.com/auth/userinfo.email\"\n" +
    "        ],\n" +
    "        \"odnoklassnikiRequestScopesAsArray\": [],\n" +
    "        \"ssoUrl\": \"https://sso-mgf.demo.rooxteam.com:443/sso\",\n" +
    "        \"yandexAppId\": \"26922e9276ee4eb9aa48168f1c945e31\",\n" +
    "        \"googleAppId\": \"533154898052-29gdhdbof0k7f4j6l06fpu192h13okig.apps.googleusercontent.com\",\n" +
    "        \"isBlocked\": false,\n" +
    "        \"esiaRequestUri\": \"https://esia-portal1.test.gosuslugi.ru/aas/oauth2/ac?state=3c6475a6-2ca5-4b10-a506-8ca497f6bec8&client_id=OPG&response_type=code&access_type=offline&scope=http%3A%2F%2Fesia.gosuslugi.ru%2Fusr_inf&timestamp=2019.01.28+11%3A39%3A17+%2B0000&redirect_uri=https%3A%2F%2Fsso-mgf.demo.rooxteam.com%3A443%2Fsso%2Fesia_callback.jsp&client_secret=MIIGHgYJKoZIhvcNAQcCoIIGDzCCBgsCAQExDzANBglghkgBZQMEAgEFADBvBgkqhkiG9w0BBwWgYgRgaHR0cDovL2VzaWEuZ29zdXNsdWdpLnJ1L3Vzcl9pbmYyMDE5LjAxLjI4IDExOjM5OjE3ICswMDAwT1BHM2M2NDc1YTYtMmNhNS00YjEwLWE1MDYtOGNhNDk3ZjZiZWM4oIIDyzCCA8cwggKvoAMCAQICCQDIBZft%2B%2B498TANBgkqhkiG9w0BAQsFADB6MQswCQYDVQQGEwJSVTEPMA0GA1UECAwGbW9za3ZhMQ0wCwYDVQQHDARjaXR5MS0wKwYDVQQKDCRsaWNobnl5LWthYmluZXQtcGF5c2hpa2EtdnRiLXIwM25sMDIxHDAaBgkqhkiG9w0BCQEWDWVkb0B2dGJyZWcucnUwHhcNMTgwODI5MTIyNzUyWhcNMjgwODI2MTIyNzUyWjB6MQswCQYDVQQGEwJSVTEPMA0GA1UECAwGbW9za3ZhMQ0wCwYDVQQHDARjaXR5MS0wKwYDVQQKDCRsaWNobnl5LWthYmluZXQtcGF5c2hpa2EtdnRiLXIwM25sMDIxHDAaBgkqhkiG9w0BCQEWDWVkb0B2dGJyZWcucnUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDGJeBLQzJQiwPajpXC448eDoyICVBZXrJ3vJqQYGac%2B4O36w2ZAGAeiw7LqBoT1L6Z7%2FprWAiltMe%2FXGEb7zOXSJUJ2rtZ42P3rdYlYt3Xlo0yftH9mOj75QIPw9631F3rBuQd16yH0f1mD2mFPg6FQNAdOsPVt5kHJLhXNtK6Py2%2FaP%2BLywHf1uhUKXF5Hk8h6vlmLfZyt2gSYro6kyCmSKdKYKNWU7O5p7uDabmvobLJpmOqlDXEr6AOAqm7mH%2F5X3AooCfbJczJAVFrtn9gfNPHMzm6CcP6G2ZfPQ1B%2FxluzSdGvMByWkqu1zC%2F%2Brqy8yBG81ot2LvGODJ5pBajAgMBAAGjUDBOMB0GA1UdDgQWBBQWM04f71%2BFqxSRB1Cn0%2FNWZceJGjAfBgNVHSMEGDAWgBQWM04f71%2BFqxSRB1Cn0%2FNWZceJGjAMBgNVHRMEBTADAQH%2FMA0GCSqGSIb3DQEBCwUAA4IBAQB4LsU%2Bhh%2Fx4pKvA4by%2F69cUUQLlPVtJHFRuNO1xO%2BVz42chDcMYLZtC%2FSrxRs3XiyUc2VCQ37jWAOY61J4j%2FMRZY51M4NS%2FO%2FkzMDdKe%2Fy9rVVXVCsWNgFgYySSP9COsTxXOTFW%2FcXFhNICWkp87o74ZCGIl%2Fh2GRjJZzt8k7pieIVUkZqimiE7j0MNNAaSMqHYb7UAy3q5yCcAwYS04P2RQeXygAJDxHBvTf6r%2FKr%2FyDwW4gRg1Mz%2F2lWtkz6uMNMqQj%2BJEtIDt1uKk5hnevbsCLFltLM%2Fxlbd0luKsYa6ibp8eaS5jZlXhEDLAY5tP7ynxn8fc%2BjFDFRWf3UhlMMMYIBszCCAa8CAQEwgYcwejELMAkGA1UEBhMCUlUxDzANBgNVBAgMBm1vc2t2YTENMAsGA1UEBwwEY2l0eTEtMCsGA1UECgwkbGljaG55eS1rYWJpbmV0LXBheXNoaWthLXZ0Yi1yMDNubDAyMRwwGgYJKoZIhvcNAQkBFg1lZG9AdnRicmVnLnJ1AgkAyAWX7fvuPfEwDQYJYIZIAWUDBAIBBQAwDQYJKoZIhvcNAQEBBQAEggEAhn5hvT0ZscGXiFOTYrmaYz2lvzF%2BmTN9EEX%2FrBcn%2BWX0djE9Hhal166HQxUfap2YsKv4Kl%2BFvQxPa0JXhYLs9nUG%2FS8U61L5Mng7pxMEID9x4E8bV3c%2BmZJQYXy27hm4roW4cp2r83sRNnumNSkitiX1Z1PFjb5dSwwY%2BhizBAgbJj9sLkTSOcMX98WkCBwb1s7U4G2vFP%2FVDISRySBA%2FKgGBrU02RSoB18d%2BPNCIw%2BhojATc0Co2%2BkLA%2FLmOP7f0CIW4RGy10ufMNQPgKdsk5KHezjpgiDI6PIIBlQI8BpEvMOoMrdHI7K%2BBSvoEivAGJBUozFeOJvQRs0K1XxSng%3D%3D&display=popup\",\n" +
    "        \"mailRuAppId\": \"762165\",\n" +
    "        \"esiaRedirectUri\": \"/esia_callback.jsp\",\n" +
    "        \"facebookRequestScopesAsArray\": [],\n" +
    "        \"vkontakteAppId\": \"5735635\",\n" +
    "        \"yandexRedirectUri\": \"/yandex_callback.jsp\",\n" +
    "        \"esiaAppId\": \"03NL02\",\n" +
    "        \"googleRedirectUri\": \"/google_callback.jsp\",\n" +
    "        \"vkontakteRedirectUri\": \"/vk_callback.jsp\",\n" +
    "        \"vkontakteRequestScopesAsArray\": [],\n" +
    "        \"odnoklassnikiAppId\": \"1248940288\",\n" +
    "        \"odnoklassnikiRedirectUri\": \"/ok_callback.jsp\",\n" +
    "        \"mailRuRedirectUri\": \"/mailru_callback.jsp\",\n" +
    "        \"esiaRequestScopesAsArray\": [\n" +
    "            \"http://esia.gosuslugi.ru/usr_inf http://esia.gosuslugi.ru/usr_avt\"\n" +
    "        ],\n" +
    "        \"yandexRequestScopesAsArray\": []\n" +
    "    },\n" +
    "    \"form\": {\n" +
    "        \"name\": \"loginForm\",\n" +
    "        \"fields\": {\n" +
    "            \"password\": {\n" +
    "                \"constraints\": [\n" +
    "                    {\n" +
    "                        \"name\": \"NotNull\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"name\": \"Size\",\n" +
    "                        \"attributes\": {\n" +
    "                            \"min\": 1,\n" +
    "                            \"max\": 1024\n" +
    "                        }\n" +
    "                    }\n" +
    "                ]\n" +
    "            },\n" +
    "            \"username\": {\n" +
    "                \"constraints\": [\n" +
    "                    {\n" +
    "                        \"name\": \"NotNull\"\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"name\": \"FilteredSize\",\n" +
    "                        \"attributes\": {\n" +
    "                            \"min\": 10,\n" +
    "                            \"max\": 10,\n" +
    "                            \"skip\": \"(^[^9]+)|([^0-9])\",\n" +
    "                            \"message\": \"symbols {skip} should be filtered out, and resulting string should have length between {min} and {max}\"\n" +
    "                        }\n" +
    "                    },\n" +
    "                    {\n" +
    "                        \"name\": \"Size\",\n" +
    "                        \"attributes\": {\n" +
    "                            \"min\": 10,\n" +
    "                            \"max\": 25\n" +
    "                        }\n" +
    "                    }\n" +
    "                ]\n" +
    "            }\n" +
    "        },\n" +
    "        \"errors\": []\n" +
    "    },\n" +
    "    \"serverUrl\": \"https://sso-mgf.demo.rooxteam.com:443/sso/auth/login-widget-router\",\n" +
    "    \"step\": \"auth_form\"\n" +
    "}"
    
    static let API_RESPONSE_OTP_REQUIRED = " {\n" +
    "   \"step\": \"enter_otp_form\",\n" +
    "   \"execution\": \"30cfc0f9-b1d4-4820-a476-06056f140e12_AAABFQAAAQBFRxdnz6ys\",\n" +
    "   \"serverUrl\": \"https://sso.rooxteam.com/sso/auth/_otp-sms\",\n" +
    "   \"form\": {\n" +
    "     \"fields\": {\n" +
    "       \"otpCode\": {\n" +
    "         \"constraints\": [\n" +
    "           {\n" +
    "             \"name\": \"NotNull\"\n" +
    "           },\n" +
    "           {\n" +
    "             \"attributes\": {\n" +
    "\"max\": 4,\n" +
    "\"min\": 4 },\n" +
    "             \"name\": \"Size\"\n" +
    "           },\n" +
    "           {\n" +
    "             \"attributes\": {\n" +
    "               \"flags\": []\n" +
    "             },\n" +
    "             \"name\": \"Pattern\"\n" +
    "           }\n" +
    "] }\n" +
    "     },\n" +
    "     \"errors\": [],\n" +
    "     \"name\": \"otpForm\"\n" +
    "}, \"view\": {\n" +
    "     \"msisdn\": \"123123\",\n" +
    "     \"isBlocked\": false,\n" +
    "     \"nextOtpCodePeriod\": 1,\n" +
    "     \"blockedFor\": 0,\n" +
    "     \"expireOtpCodeTime\": 1,\n" +
    "     \"otpCodeAvailableAttempts\": 1\n" +
    "} }"
    static let API_RESPONSE_OTP_MSISDN_FORM = "{\n" +
    "\"step\":\"login-by-otp-form\",\n" +
    "\"execution\":\"2787d6ac-d397-435a-a6f5-b2dd72f11725\",\n" +
    "\"serverUrl\":\"http://sso-mgf.demo.rooxteam.com:8080/sso/auth/_login-by- otp\",\n" +
    "\"form\":{\n" +
    "\"fields\":{\n" +
    "\"msisdn\":{\n" +
    "\"constraints\":[\n" +
    "{\n" +
    "\"name\":\"NotNull\"\n" +
    "},\n" +
    "{\n" +
    "\"attributes\":{\n" +
    "\"max\":10,\n" +
    "\"message\":\"symbols {skip} should be filtered out, and resulting string should have length between {min} and {max}\",\n" +
    "\"skip\":\"(^[^9]+)|([^0-9])\",\n" +
    "\"min\":10\n" +
    "},\n" +
    "\"name\":\"FilteredSize\"\n" +
    "},\n" +
    "{\n" +
    "\"attributes\":{\n" +
    "\"max\":25,\n" +
    "\"min\":10\n" +
    "},\n" +
    "\"name\":\"Size\"\n" +
    "}\n" +
    "]\n" +
    "}\n" +
    "},\n" +
    "\"errors\":[\n" +
    "],\n" +
    "\"name\":\"form\"\n" +
    "}\n" +
    "}"
}
