//
//  NetworkFaker.swift
//  RooxSDKTests
//
//  Created by Alexandr Timoshenko on 2/27/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation
@testable import RooxSDK
class EmptyNetwork:NetworkLayerProtocol{
    func perform(url: URL, formData: [String : String], headers: [String : String], success: @escaping (Data) -> Void, failure: @escaping (Error) -> Void) {
        failure(NotHandled())
    }
}

protocol ValueValidatorProtocol {
    func validate(headerValues:[String:String])throws
}

class SimpleValueValidator:ValueValidatorProtocol{
    let values:[String:String]
    init(values:[String:String]) {
        self.values = values
    }
    
    func validate(headerValues: [String : String]) throws {
        try self.values.forEach { (arg0) in
            let (key, value) = arg0
            let expectedValue = value
            let realValue = headerValues[key] ?? ""
            if (expectedValue != realValue){
                throw ValueInvalid()
            }
        }
    }
}

protocol RequestHandlerProtocol{
    func handle(url:String,formData: [String : String], headers: [String : String]) -> String?
}

class SimpleRequestHandler:RequestHandlerProtocol{
    let calls:[String:String]
    init(calls:[String:String]) {
        self.calls = calls
    }
    
    func handle(url: String, formData: [String : String], headers: [String : String]) -> String? {
        return calls[url]
    }
}

class ValueInvalid:Error{}
class FormInvalid:Error{}
class NotHandled:Error{}
class Handled:Error{}
class FakeNetworkLayer: NetworkLayerProtocol{
    var headerValidators:[ValueValidatorProtocol]
    var formValidators:[ValueValidatorProtocol]
    var requestHandlers: [RequestHandlerProtocol]
    init(header:[ValueValidatorProtocol],form:[ValueValidatorProtocol],request:[RequestHandlerProtocol]) {
        self.headerValidators = header
        self.formValidators = form
        self.requestHandlers = request
    }
    func perform(url: URL, formData: [String : String], headers: [String : String], success: @escaping (Data) -> Void, failure: @escaping (Error) -> Void) {
        do{
            try headerValidators.forEach { (validator:ValueValidatorProtocol) in
                do{
                    try validator.validate(headerValues: headers)
                }catch {
                    failure(ValueInvalid())
                    throw Handled()
                }
            }
            try formValidators.forEach { (validator:ValueValidatorProtocol) in
                do{
                    try validator.validate(headerValues: formData)
                }catch {
                    failure(FormInvalid())
                    throw Handled()
                }
            }
            try requestHandlers.forEach { (handler:RequestHandlerProtocol) in
                let ret = handler.handle(url: url.path,formData: formData,headers: headers)
                if (ret != nil){
                    success(ret!.data(using: String.Encoding.utf8)!)
                    throw Handled()
                }
            }
            failure(NotHandled())
        }catch{
            
        }
        
    }
}
struct Samples{
    static let TokenGetOkRequest = SimpleRequestHandler(calls: ["/sso/oauth2/access_token":ApiResponses.API_RESPONSE_TOKEN_OK])
static let AutologinOkRequest = SimpleRequestHandler(calls: ["/sso/auth/autologin":ApiResponses.API_RESPONSE_JWT_OK,"/sso/oauth2/access_token":ApiResponses.API_RESPONSE_TOKEN_OK])
static let AutologinFailAccessTokenRequest = SimpleRequestHandler(calls: ["/sso/auth/autologin":ApiResponses.API_RESPONSE_JWT_OK])

static let AutologinUserNameRequest = SimpleRequestHandler(calls: ["/sso/auth/autologin":ApiResponses.API_RESPONSE_JWT_OK,"/sso/oauth2/access_token":ApiResponses.API_RESPONSE_USERNAME_PASS_REQUIRED])
    static let GetTokenUserNameRequestOK = SimpleRequestHandler(calls: ["/sso/oauth2/access_token":ApiResponses.API_RESPONSE_TOKEN_OK])
    static let GetTokenUserNameRequestWrong = SimpleRequestHandler(calls: ["/sso/oauth2/access_token":ApiResponses.API_RESPONSE_USERNAME_PASS_REQUIRED])
    
    //otp
    static let AutologinOTPRequest = SimpleRequestHandler(calls: ["/sso/auth/autologin":ApiResponses.API_RESPONSE_JWT_OK,"/sso/oauth2/access_token":ApiResponses.API_RESPONSE_OTP_REQUIRED])
    static let AutologinOTPForm = SimpleRequestHandler(calls: ["/sso/auth/autologin":ApiResponses.API_RESPONSE_JWT_OK,"/sso/oauth2/access_token":ApiResponses.API_RESPONSE_OTP_MSISDN_FORM])
}
