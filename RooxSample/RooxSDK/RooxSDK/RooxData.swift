//
//  RooxData.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation

/// Structure with access token information
public class AccessTokenInfo:Decodable{
    public let authType:String
    public let realm:String
    public let sub:String
    public let roles:[String]
    public let auth_level:String
    public let expires_in:Int
    public let token_type:String
    public let client_id:String
    public let scope:[String]
    public let cn:String
    enum CodingKeys: String, CodingKey {
        case authType = "authType"
        case realm = "realm"
        case sub = "sub"
        case roles = "roles"
        case auth_level = "auth_level"
        case expires_in = "expires_in"
        case token_type = "token_type"
        case client_id = "client_id"
        case scope = "scope"
        case cn = "cn"
    }
    public var description: String {
        return "\n{\n authType: \(self.authType),\n"
            + " realm: \(self.realm),\n"
            + " sub: \(self.sub),\n"
            + " roles: \(self.roles),\n"
            + " expires_in: \(self.expires_in),\n"
            + " token_type: \(self.token_type),\n"
            + " client_id: \(self.client_id),\n"
            + " scope: \(self.scope),\n"
            + " cn: \(self.cn),\n"
            + " auth_level: \(self.auth_level )\n}"
    }
}
/// Structure to store token for api access
public class Token{
    public let token:String
    init(token:String) {
        self.token = token
    }
    public static func create(token:String)->Token{
        return Token(token:token)
    }
}

/// Response received from autologin api endpoint
public struct AutoLoginResponse:Decodable{
    let jwt:String
    enum CodingKeys: String, CodingKey {
        case jwt = "auto-login-jwt"
    }
    
}

/// Struct to hold success token api endpoint response
public struct AccessTokenResponse:Decodable {
    let accessToken:String
    let refreshToken:String
    let expiresIn: Int
    let jwt:String
    public let mpt:String
    let mptExpiresIn:Int?
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case expiresIn = "expires_in"
        case jwt = "JWTToken"
        case mpt = "mpt"
        case mptExpiresIn = "mpt_expires_in"
    }
    
    public func storeToken()->Token{
        return Token(token:mpt)
    }
    public func apiToken()->Token{
        return Token(token:accessToken)
    }
}

/// Information about authentication step and additional information required to continue authentication
public struct AuthenticationResponse:Decodable{
    public let execution:String
    /// Name of step
    public let step:String
    /// information about step view
    public let view: AuthenticationView?
    /// information about step form
    public let form: AuthenticationForm
}

/// Information about Authentication form
public struct AuthenticationForm:Decodable{
    /// name of authentication form
    public let name:String
    /// authentication errors
    public let errors:[FormError]
    /// fields required to continue authentication
    public let fields:[String:FormField]
}

/// Holder for authentication error
public struct FormError:Decodable{
    /// Error message
    public let message:String
}
public struct FormField:Decodable{
    public let constraints: [FieldConstraint]
}

public struct FieldConstraint: Decodable{
    let name:String
    enum CodingKeys: String, CodingKey {
        case name = "name"
    }
}

/// Information about Authentication view
public struct AuthenticationView:Decodable{
    public let captchaUrl:String?
    public let isBlocked:Bool?
    public let blockedFor:Int?
    public let nextOtpCodePeriod:Int?
    public let expireOtpCodeTime:Int?
    public let otpCodeAvailableAttempts: Int?
    public let msisdn:String?
}
