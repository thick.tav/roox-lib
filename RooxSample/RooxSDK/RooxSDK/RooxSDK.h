//
//  RooxSDK.h
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for RooxSDK.
FOUNDATION_EXPORT double RooxSDKVersionNumber;

//! Project version string for RooxSDK.
FOUNDATION_EXPORT const unsigned char RooxSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RooxSDK/PublicHeader.h>


