//
//  RooxAuth.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation

///Base protocol to describe low level API
public protocol RooxApiProtocol{
    /// Method to call to check if autologin available
    /// - Parameter success: block to be called with `AutoLoginResponse` if autologin success
    /// - Parameter failure: block to be called with `Error` if autologin failed
    func autologin(success:@escaping (AutoLoginResponse)->Void, failure: @escaping (Error)->Void)
    /// Method to call to perform main authenticator state machine on API
    /// - Parameter formData: dictionary to send to api endpoint
    /// - Parameter success: block to be called with `AccessTokenResponse` if login success
    /// - Parameter failure: block to be called with `Error` if request failed
    func getAccessToken(formData: [String:String], success: @escaping (AccessTokenResponse)->Void, failure: @escaping (Error)->Void)
    
    /// Method to call to get information about access token
    /// - Parameter token: token got from sdk
    /// - Parameter success: block to be called with `AccessTokenInfo`
    /// - Parameter failure: block to be called with `Error` if request failed
    func getTokenInfo(token:String, success: @escaping (AccessTokenInfo)->Void, failure: @escaping (Error)->Void)
}

/// Base protocol for different types of authentications
public protocol RooxAuthenticatorProtocol{
    /// Entry point for authentication, checks if can authenticate automatically
    /// - Parameter success: block to be called with `Token` if logged in successfully
    /// - Parameter failure: block to be called with `Error` if login failed
    func authenticate(success: @escaping (Token)->Void, failure: @escaping (Error)->Void)
    
    /// Entry point for authentication with parameters
    /// - Parameter formData: dictionary to send to authentication endpoint
    /// - Parameter success: block to be called with `Token` if logged in successfully
    /// - Parameter failure: block to be called with `Error` if login failed
    func authenticate(formData: [String:String],success: @escaping (Token)->Void, failure: @escaping (Error)->Void)
    
    /// Entry point to continue authentification with data required by `AuthenticateError`
    /// - Parameter formData: dictionary to send to authentication endpoint
    /// - Parameter success: block to be called with `Token` if logged in successfully
    /// - Parameter failure: block to be called with `Error` if login failed
    func continueAuthenticate(formData: [String:String], success: @escaping (Token)->Void, failure: @escaping (Error)->Void)
}

public class EventBuilder{
    var properties: [String:String]
    let eventName: String
    init(eventName: String){
        self.properties = [:]
        self.eventName = eventName
    }
    func addProp(key:String, value:String)->EventBuilder{
        properties[key] = value
        return self;
    }
}

public protocol RooxAnalyticsProtocol{
    func track(eventName:String, eventParameters:[String:String?]);
    func track(event:EventBuilder);
}

/// Base protocol to provide local storage for token information
public protocol RooxStorageProtocol {

    /// Store remote token locally
    /// - Parameter token: data to store
    func store(token:Token)
    
    /// Fetch locally stored `Token`
    /// - Returns: `Token` or nil if not exists
    func load()->Token?
    
    ///Removes locally stored token
    func remove()
    
    /// Raw api to store keys for runtime preferences
    func storePref(key:String,value:String)
    
    /// Raw api to load keys for runtime preferences
    func loadPref(key:String)->String

}
