//
//  RooxApi.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation
protocol NetworkLayerProtocol {
    func perform(url:URL,formData:[String:String],headers:[String:String],success:@escaping (Data,URLResponse?)->Void,failure: @escaping(Error)->Void)
    func performGet(url:URL,headers:[String:String],success:@escaping (Data,URLResponse?)->Void,failure: @escaping(Error)->Void)
}
public struct NetworkResponse{
    let data:String
    let response:URLResponse
}
internal class DefaultNetwork: NetworkLayerProtocol{
    let _config:Config
    let rooxAnalytics:RooxAnalyticsProtocol
    init(config:Config) {
        self._config = config
        self.rooxAnalytics = RooxAnalytics(config: config, storage: RooxStore(config: config))
    }
    func performWithRetry(session: URLSession,request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void,lastError:Error?, attempt: Int){
        if (attempt < _config.retryCount){
            session.dataTask(with: request,completionHandler: { (data:Data?, response:URLResponse?, error:Error?) in
                
                DispatchQueue.main.async {
                    if (error != nil){
                        self.performWithRetry(session: session, request: request, completionHandler: completionHandler, lastError: error, attempt: attempt+1)
                    }else if (data != nil){
                        let dataString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                        print(dataString)
                        completionHandler(data,response,error)
                    }
                }
            }).resume()
        }else{
            completionHandler(nil,nil,lastError)
        }
    }
    func perform(url: URL, formData: [String : String], headers: [String : String], success: @escaping (Data,URLResponse?) -> Void, failure: @escaping (Error) -> Void) {
       
        var request = URLRequest(url: url)
        print("Perform request to ",url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        if (_config.keepAlive){
            request.setValue("true", forHTTPHeaderField: "keep-alive")
        }
        request.httpMethod = "POST"
        request.encodeParameters(parameters: formData)
        headers.forEach { (arg0) in
            let (key, value) = arg0
            request.addValue(value, forHTTPHeaderField: key)
        }
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = _config.connectTimeout
        sessionConfig.timeoutIntervalForResource = _config.readTimeout
        
        let session = URLSession(configuration: sessionConfig)
        
        performWithRetry(session: session, request: request, completionHandler: { (data:Data?, response:URLResponse?, error:Error?) in
            DispatchQueue.main.async {
                if (error != nil){
                    failure(error!)
                }else if (data != nil){
                    let dataString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                    print(dataString)
                    success(data!,response)
                }
            }
        }, lastError: nil, attempt: 0)
    }
    func performGet(url: URL,headers: [String : String], success: @escaping (Data,URLResponse?) -> Void, failure: @escaping (Error) -> Void) {
        
        var request = URLRequest(url: url)
        print("Perform request to ",url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        if (_config.keepAlive){
            request.setValue("true", forHTTPHeaderField: "keep-alive")
        }
        request.httpMethod = "GET"        
        headers.forEach { (arg0) in
            let (key, value) = arg0
            request.addValue(value, forHTTPHeaderField: key)
        }
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = _config.connectTimeout
        sessionConfig.timeoutIntervalForResource = _config.readTimeout
        
        let session = URLSession(configuration: sessionConfig)
        
        
        performWithRetry(session: session, request: request, completionHandler: { (data:Data?, response:URLResponse?, error:Error?) in
            DispatchQueue.main.async {
                if (error != nil){
                    failure(error!)
                }else if (data != nil){
                    let dataString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                    print(dataString)
                    success(data!,response)
                }
            }
        }, lastError: nil, attempt: 0)
    }
}

extension URLRequest {
    
    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    mutating func encodeParameters(parameters: [String : String]) {
        httpMethod = "POST"
        
        let parameterArray = parameters.map { (arg) -> String in
            let (key, value) = arg
            return "\(key)=\(self.percentEscapeString(value))"
        }
        let params = parameterArray.joined(separator: "&")
        print("Request Params: ", params)
        let data = params.data(using: String.Encoding.utf8)
        httpBody = data
    }
}



internal class RooxApi: RooxApiProtocol{
    let decoder = JSONDecoder()
    let _config:Config
    internal var _network: NetworkLayerProtocol
    let _analytics: RooxAnalyticsProtocol
    var lastExecutionId:String
    init(config:Config, network:NetworkLayerProtocol,analytics:RooxAnalyticsProtocol) {
        self._config = config
        self._network = network
        self._analytics = analytics
        self.lastExecutionId = ""
    }

    func getTokenInfo(token: String, success: @escaping (AccessTokenInfo) -> Void, failure: @escaping (Error) -> Void) {
        let url = URL(string: String(format: "%@/sso/oauth2/tokeninfo?access_token=%@", self._config.url,token))!
        self._network.performGet(url: url, headers: [:], success: { (data:Data, response:URLResponse?) in
            let resp = try? self.decoder.decode(AccessTokenInfo.self, from: data)
            if (resp != nil){
                success(resp!)
            }else{
                failure(ServerError())
            }
        }) { (error:Error) in
            failure(error)
        }
        
    }
    func performRequest(action:String,formData:[String:String],headers:[String:String],success:@escaping (Data,URLResponse?)->Void,failure: @escaping(Error)->Void){
        let url = URL(string: String(format: "%@/%@", self._config.url,action))!
        
        self._network.perform(url: url, formData: formData, headers: headers, success: {(data:Data,response:URLResponse?) in
            let httpResp = response as? HTTPURLResponse
            self._analytics.track(event:
                EventBuilder(eventName: "SSO_request")
                    .addProp(key: "Request-Url", value: url.absoluteString)
                    .addProp(key: "Response-Code", value: String(httpResp?.statusCode ?? 0))
                    .addProp(key: "Response-Content-Type", value: httpResp?.allHeaderFields["Content-Type"].debugDescription ?? "")
                    .addProp(key: "Response-Content-Length", value: httpResp?.allHeaderFields["Content-Length"].debugDescription ?? "")
            )
            
            success(data,response)
        }, failure: {(error:Error) in
            failure(error)
        })
    }
    
    func autologin(success: @escaping (AutoLoginResponse) -> Void, failure: @escaping (Error) -> Void) {
        performRequest(action: "sso/auth/autologin", formData: [:], headers: self._config.headers, success: { (data:Data,response:URLResponse?) in
        
            let resp = try? self.decoder.decode(AutoLoginResponse.self, from: data)
            if (resp != nil){
                success(resp!)
            }else{
                failure(ServerError())
            }
        }) { (error:Error) in
            failure(error)
        }
    }
    
    func handleAuthSuccess(token:AccessTokenResponse, success: @escaping (AccessTokenResponse)->Void){
        getTokenInfo(token: token.accessToken, success: { (tokenInfo:AccessTokenInfo) in
            self._analytics.track(event:
                EventBuilder(eventName: "AuthSuccess")
                    .addProp(key: "Auth-Sub", value: tokenInfo.sub)
                    .addProp(key: "Auth-ExecutionId", value: self.lastExecutionId)
            )
        }) { (error:Error) in
            
        }
       
        success(token)
    }
    func getAccessToken(formData: [String : String], success: @escaping (AccessTokenResponse) -> Void, failure: @escaping (Error) -> Void) {
        performRequest(action: "sso/oauth2/access_token", formData: formData.merging(["grant_type":"urn:roox:params:oauth:grant-type:m2m",
                                                                                      "realm":"/customer",
                                                                                      "client_id":self._config.clientId,
                                                                                      "client_secret":self._config.clientSecret], uniquingKeysWith: { (k1:String, _) -> String in
            k1
        }), headers: [:], success: { (data:Data,response:URLResponse?) in
            let token = try? self.decoder.decode(AccessTokenResponse.self, from: data)
            if (token != nil){
                self.handleAuthSuccess(token: token!, success: success)
            }else{
                let authExp = try? self.decoder.decode(AuthenticationResponse.self, from: data)
                if (authExp != nil){
                    self.lastExecutionId = authExp?.execution ?? ""
                    self._analytics.track(event:
                        EventBuilder(eventName: "AuthFailed")
                            .addProp(key: "Auth-Step", value: authExp?.step ?? "")
                            .addProp(key: "Auth-ExecutionId", value: self.lastExecutionId)
                    )
                    
                    failure(AuthenticateError(authForm: authExp!))
                }else{
                    failure(ServerError())
                }
                
            }
        }) { (error:Error) in
            failure(error)
        }
    }
}
