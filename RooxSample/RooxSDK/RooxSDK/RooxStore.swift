//
//  RooxStore.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/8/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation

internal class RooxStore: RooxStorageProtocol{
    let keychain = KeychainSwift()
    let defaults = UserDefaults.standard
    init(config:Config) {
        keychain.accessGroup = config.keyChainGroup
    }
    
    public func load() -> Token? {
        let token = keychain.get("mpt") ?? defaults.string(forKey: "mpt")
        if (token != nil){
            return Token(token:token!)
        }
        return nil
    }
    
    public func remove() {
        keychain.delete("mpt")
        defaults.removeObject(forKey: "mpt")
    }
    public func store(token: Token) {
        if (!keychain.set(token.token, forKey: "mpt")){
            defaults.set(token.token, forKey: "mpt")
        }
    }
    
    public func storePref(key: String, value: String) {
        if (!keychain.set(value, forKey: key)){
            //use nsdefaults
            defaults.set(value, forKey: key)
        }
    }
    
    public func loadPref(key: String) -> String {
        return keychain.get(key) ?? defaults.string(forKey: key) ?? ""
    }
    
}
