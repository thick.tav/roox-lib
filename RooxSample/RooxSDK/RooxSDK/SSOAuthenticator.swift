//
//  SSOAuthenticator.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation

internal class SSOAuthenticator: RooxAuthenticatorProtocol{
    let _api:RooxApiProtocol
    var lastAuthError: AuthenticateError? = nil
    let _store: RooxStorageProtocol
    init(store:RooxStorageProtocol, api:RooxApiProtocol){
        self._api = api
        self._store = store
    }
    func authenticate(success: @escaping(Token) -> Void,  failure: @escaping (Error) -> Void) {
        if (self._store.loadPref(key: UIDM.PREF_AUTOLOGIN_BLOCK).isEmpty || self._store.loadPref(key: UIDM.PREF_AUTOLOGIN_BLOCK)=="NO"){
            _api.autologin(success: { (token:AutoLoginResponse) in
                self._api.getAccessToken(formData: ["auto-login-jwt":token.jwt,"service":"dispatcher"], success: { (token:AccessTokenResponse) in
                    self._store.store(token: token.storeToken())
                    success(token.apiToken())
                }, failure: { (error:Error) in
                    if (error is AuthenticateError){
                        self.lastAuthError = error as? AuthenticateError
                    }
                    failure(error)
                })
            }) { (error:Error) in
                if (error is AuthenticateError){
                    self.lastAuthError = error as? AuthenticateError
                }
                failure(error)
            }
        }else{
            self._api.getAccessToken(formData: ["service":"dispatcher"], success: { (token:AccessTokenResponse) in
                self._store.store(token: token.storeToken())
                success(token.apiToken())
            }, failure: { (error:Error) in
                if (error is AuthenticateError){
                    self.lastAuthError = error as? AuthenticateError
                }
                failure(error)
            })
        }
    }
    
    func authenticate(formData: [String : String], success: @escaping (Token) -> Void, failure: @escaping (Error) -> Void) {
        let data = ["service":"dispatcher"].merging(formData) { (k1, k2) -> String in
            k1
        }
        self._api.getAccessToken(formData: data, success: { (token:AccessTokenResponse) in
            self._store.store(token: token.storeToken())
            success(token.apiToken())
        }, failure: { (error:Error) in
            if (error is AuthenticateError){
                self.lastAuthError = error as? AuthenticateError
                self.continueAuthenticate(formData: formData, success: success, failure: failure)
                return;
            }
            failure(error)
        })
    }
    
    func continueAuthenticate(formData: [String : String], success: @escaping (Token) -> Void, failure: @escaping (Error) -> Void) {
        let execution = lastAuthError?.authForm.execution ?? ""
        let requestData = formData.merging(["_eventId":"next","service":"dispatcher","execution":execution]) { (k1:String, _) -> String in
            k1
        }
        self._api.getAccessToken(formData: requestData, success: { (token:AccessTokenResponse) in
            self._store.store(token: token.storeToken())
            success(token.apiToken())
        }) { (error:Error) in
            if (error is AuthenticateError){
                self.lastAuthError = error as? AuthenticateError
            }
            failure(error)
        }
    }
}
