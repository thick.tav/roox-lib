//
//  RooxAnalytics.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 4/3/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony


class TelemetryStorageSequence : Sequence, IteratorProtocol {
    typealias Element = EventData
    
    private let directoryEnumerator: TelemetryDirectoryEnumerator?
    
    private var currentPing: EventData?
    private var currentPingFile: URL?
    
    init(directoryEnumerator: TelemetryDirectoryEnumerator?) {
        self.directoryEnumerator = directoryEnumerator
    }
    static func daysOld(date: Date) -> Int {
        let end = dateFromTimestamp(Date().timeIntervalSince1970)
        return Calendar.current.dateComponents([.day], from: date, to: end).day ?? 0
    }
    static func dateFromTimestamp(_ timestampSince1970: TimeInterval) -> Date {
        return Date(timeIntervalSince1970: timestampSince1970)
    }
    
    func isStale(pingFile: URL) -> Bool {
        guard let time = RooxAnalytics.extractTimestampFromName(pingFile: pingFile) else {
            return false
        }
        
        let days = TelemetryStorageSequence.daysOld(date: time)
        return days > 1
    }
    
    func next() -> EventData? {
        guard let directoryEnumerator = self.directoryEnumerator else {
            return nil
        }
        
        while let url = directoryEnumerator.nextObject() as? URL {
            if isStale(pingFile: url) {
                remove(pingFile: url)
                continue
            }
            
            do {
                let data = try Data(contentsOf: url)
                if let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                    let ping = EventData.from(dictionary: dict) {
                    currentPingFile = url
                    return ping
                } else {
                    print("TelemetryStorageSequence.next(): Unable to deserialize JSON in file \(url.absoluteString)")
                }
            } catch {
                print("TelemetryStorageSequence.next(): \(error.localizedDescription)")
            }
            
            // If we get here without returning a ping, something went wrong that
            // is unrecoverable and we should just delete the file.
            remove(pingFile: url)
        }
        
        currentPingFile = nil
        return nil
    }
    
    func remove() {
        guard let currentPingFile = self.currentPingFile else {
            return
        }
        
        remove(pingFile: currentPingFile)
    }
    
    private func remove(pingFile: URL) {
        do {
            try FileManager.default.removeItem(at: pingFile)
        } catch {
            print("TelemetryStorageSequence.removePingFile(\(pingFile.absoluteString)): \(error.localizedDescription)")
        }
    }
}

class TelemetryDirectoryEnumerator: NSEnumerator {
    private let contents: [URL]
    
    private var index = 0
    
    init(directory: URL) {
        var contents: [URL]
        
        do {
            contents = try FileManager.default.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil, options: .skipsHiddenFiles).sorted(by: { (a, b) -> Bool in
                return a.lastPathComponent < b.lastPathComponent
            })
        } catch {
            print("TelemetryDirectoryEnumerator(directory: \(directory)): \(error.localizedDescription)")
            contents = []
        }
        
        self.contents = contents
        
        super.init()
    }
    
    override func nextObject() -> Any? {
        if index < contents.count {
            let result = contents[index]
            index += 1
            return result
        }
        
        return nil
    }
}

struct EventData{
    let properties: [String:Any?]
    let eventName: String
    let ts: Double
    func toDictionary() -> [String : Any] {
        let general:[String:Any]=["timestamp": ts, "eventName": eventName, "properties":properties]
//        general.merge(properties, uniquingKeysWith: { (k1, _) -> Any in
//            k1
//        })
        return general
    }
    static func from(dictionary:[String:Any]) -> EventData?{
        if let timestamp = dictionary["timestamp"] as? Double,
            let eventName = dictionary["eventName"] as? String,
            let measurements = dictionary["properties"] as? [String : Any?] {
            return EventData(properties: measurements, eventName: eventName, ts: timestamp)
        }
        return nil
    }
    func measurementsJSON() -> Data? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: properties, options: [])
            return jsonData
        } catch let error {
            print("Error serializing EventData to JSON: \(error)")
            return nil
        }
    }
}

class AnalyticsStorage{
    private func directory() -> URL? {
        do {
            let url = try FileManager.default.url(for: FileManager.SearchPathDirectory.cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("roox-telemetry")
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            return url
        } catch {
            print("TelemetryStorage.directoryForPingType(): \(error.localizedDescription)")
            return nil
        }
    }
    func sequence() -> TelemetryStorageSequence {
        guard let directory = directory() else {
            print("TelemetryStorage.sequenceForPingType(): Could not get directory")
            return TelemetryStorageSequence(directoryEnumerator: nil)
        }
        
        let directoryEnumerator = TelemetryDirectoryEnumerator(directory: directory)
        return TelemetryStorageSequence(directoryEnumerator: directoryEnumerator)
    }
    func queue(eventName: String, eventParameters: [String:String?]){
        guard let directory = directory() else {
            print("TelemetryStorage.enqueue(): Could not get directory")
            return
        }
        var url = directory.appendingPathComponent("-t-\(Date().timeIntervalSince1970).json")
        let eventData = EventData(properties: eventParameters, eventName: eventName, ts: Date().timeIntervalSince1970)
        
        do {
//            var superProperties:[String:Any?] = [:]
            
            let jsonData = try JSONSerialization.data(withJSONObject: eventData.toDictionary(), options: .prettyPrinted)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                try jsonString.write(to: url, atomically: true, encoding: .utf8)
                
                print("Wrote file: \(url)")
                // Exclude this file from iCloud backups.
                var resourceValues = URLResourceValues()
                resourceValues.isExcludedFromBackup = true
                try url.setResourceValues(resourceValues)
            } else {
                print("ERROR: Unable to generate JSON data")
            }
        } catch {
            print("TelemetryStorage.enqueue(): \(error.localizedDescription)")
        }
        
    }
}
internal class RooxAnalytics:RooxAnalyticsProtocol{
    let _config:Config
    let _storage:RooxStorageProtocol
    let scheduler:TelemetryScheduler
    let analyticsStorage:AnalyticsStorage
    let trackingUrl: String
    let reachability: Reachability
    class func extractTimestampFromName(pingFile: URL) -> Date? {
        let str = pingFile.absoluteString
        let pat = "-t-([\\d.]+)\\.json"
        let regex = try? NSRegularExpression(pattern: pat, options: [])
        assert(regex != nil)
        if let result = regex?.matches(in:str, range:NSMakeRange(0, str.count)),
            let match = result.first, match.range.length > 0 {
            let time = (str as NSString).substring(with: match.range(at: 1))
            if let time = Double(time) {
                return Date(timeIntervalSince1970: time)
            }
        }
        return nil
    }
    
    init(config:Config, storage:RooxStorageProtocol) {
        self._config = config
        self._storage = storage;
        self.reachability = Reachability()!
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    
        
        self.trackingUrl = String.init(format: "%@/sso/pushreport", self._config.url)
        self.analyticsStorage = AnalyticsStorage()
        self.scheduler = TelemetryScheduler(storage: self.analyticsStorage, configuration: config)
    
    }
    
    func track(event:EventBuilder) {
        var withSuper = event.properties
        withSuper["Device-Model"] = UIDevice.modelName
        withSuper["Device-OS-Version"] =  UIDevice.current.systemVersion
        withSuper["Device-ID"] = UIDevice.current.identifierForVendor?.uuidString
        withSuper["ClientId"] = _config.clientId
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        withSuper["App-Version"] = version
        withSuper["SDK-Version"] = UIDM.SDK_VERSION
        withSuper["App-Package"] = Bundle.main.bundleIdentifier
        withSuper["Connection-WifiName"] = getWiFiSsid()
        withSuper["Connection-Type"] = getNetworkType()
        self.track(eventName: event.eventName, eventParameters: withSuper)
    }
    func getWiFiSsid() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    break
                }
            }
        }
        return ssid
    }
    func getNetworkType()->String {
        return reachability.connection.description
    }

    func track(eventName: String, eventParameters: [String:String?]) {
        let prefKey = self._storage.loadPref(key: UIDM.PREF_ANALYTICS_ENABLED);
        if (prefKey == "YES" || prefKey == ""){
            self.analyticsStorage.queue(eventName: eventName, eventParameters: eventParameters)
            self.scheduler.scheduleUpload {
                
            }
        }
    }

}

class TelemetryClient: NSObject {
    private let configuration: Config
    
    init(configuration: Config) {
        self.configuration = configuration
    }
    func reportDate() -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatterGet.string(from: Date())
    }
    func reportChecksum(data:Data) -> String{
        let str = String(data: data, encoding: .utf8) ?? ""    
        var result = 0
        for i in 0..<str.count {
            let index = str.index(str.startIndex, offsetBy: i)
            print("\(str[index]) ", terminator: "")
            var toShift:UInt8 = 0
            if (i % 3 == 0 || i % 5 == 2) {
                let chr = (str[index].asciiValue ?? 0)
                toShift = chr
                let sh = i % 24
                result ^= (Int(toShift) << (sh));
            }else{
                let chr = (str[index].asciiValue ?? 0)
                let sh = i % 24
                let nchr = ~chr;
                let andchr = (0xff & nchr)
                result ^= Int(andchr) << (sh);
            }
        }
        return String(result)
    }
    // The closure is called with an HTTP status code (zero if unavailable) and an error
    func upload(ping: EventData, completionHandler: @escaping (Int) -> Void) -> Void {
        guard let url = URL(string: "\(configuration.url)/sso/pushreport") else {
            completionHandler(0)
            return
        }
        
        guard let data = ping.measurementsJSON() else {
            completionHandler(0)
            return
        }
        
        var request = URLRequest(url: url)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = data
        request.addValue("roox-sdk", forHTTPHeaderField: "YA_REPORT_SERVICE")
        let repDate = self.reportDate();
        request.addValue(repDate, forHTTPHeaderField: "YA_REPORT_SENDING_TIME")
        
        let repSig = reportChecksum(data: data)
        request.addValue(repSig, forHTTPHeaderField: "YA_REPORT_CHECKSUM")
        request.httpShouldHandleCookies = false
        
        print("\(request.httpMethod ?? "(GET)") \(request.debugDescription)")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in            
            let httpResponse = response as? HTTPURLResponse
            let statusCode = httpResponse?.statusCode ?? 0
            completionHandler(statusCode)
        }
        task.resume()
    }
}
class TelemetryScheduler {
    private let storage: AnalyticsStorage
    
    private let client: TelemetryClient
    private let configuration: Config
    init(storage: AnalyticsStorage, configuration:Config) {
        self.configuration = configuration
        self.storage = storage
        self.client = TelemetryClient(configuration: configuration)
    }
    
    func scheduleUpload(completionHandler: @escaping () -> Void) {
        var pingSequence = storage.sequence()
        
        func uploadNextPing() {
            guard let ping = pingSequence.next() else {
                completionHandler()
                return
            }
            
            
            client.upload(ping: ping) { httpStatusCode in
                // Delete the ping on any 2xx or 4xx status code.
                if [2,4].contains(Int(httpStatusCode / 100)) {
                    // Network call completed, successful or with error, delete the ping, and upload the next ping.
                    pingSequence.remove()
                    uploadNextPing()
                } else {
                    completionHandler()
                }
            }
        }
        
        uploadNextPing()
    }

}
public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad Mini 5"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
