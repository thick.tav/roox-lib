//
//  UIDM.swift
//  sdk
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation

/**
 Class describes sdk configuration
*/
public struct Config{
    /// base url for api requests
    let url:String
    /// client id for api requests
    let clientId:String
    /// client secret for api requsts
    let clientSecret:String
    /// headers to use with autologin
    let headers: [String:String]
    /// keychain group to store data
    let keyChainGroup: String
    let connectTimeout:Double
    let readTimeout:Double
    let retryCount:UInt32
    let keepAlive:Bool
}

/**
 Builder class to create `Config` instance
*/
public class ConfigBuilder {
    var _url:String = ""
    var _clientId:String = ""
    var _clientSecret:String = ""
    var _connectTimeout:Double = 30;
    var _readTimeout:Double = 30;
    var _retryCount:UInt32 = 3;
    var _keepAlive:Bool = false;
    var _headers: [String:String] = [:]
    var _keyChainGroup: String = ""
    
    /// Creates instance of `ConfigBuilder`
    /// - Returns: new instance
    public static func create() -> ConfigBuilder{
        return ConfigBuilder()
    }
    
    /// Set base url for api requests
    /// - Parameter _url: base url
    /// - Returns: self
    public func url(_url:String)->ConfigBuilder{
        self._url = _url
        return self
    }
    
    /// Set client id for api requests
    /// - Parameter _id: client id
    /// - Returns: self
    public func clientId(_id:String)->ConfigBuilder{
        self._clientId = _id
        return self
    }
    
    /// Set client secret for api requests
    /// - Parameter _secret: client secret
    /// - Returns: self
    public func clientSecret(_secret:String)->ConfigBuilder{
        self._clientSecret = _secret
        return self
    }
    
    /// Set connect timeout in seconds
    /// - Parameter _timeout
    /// - Returns: self
    public func connectTimeout(_timeout:Double)->ConfigBuilder{
        self._connectTimeout = _timeout
        return self
    }
    
    /// Set read timeout in seconds
    /// - Parameter _timeout
    /// - Returns: self
    public func readTimeout(_timeout:Double)->ConfigBuilder{
        self._readTimeout = _timeout
        return self
    }
    
    /// Set api retry count
    /// - Parameter _timeout
    /// - Returns: self
    public func retryCount(_count:UInt32)->ConfigBuilder{
        self._retryCount = _count
        return self
    }
    
    /// Set if should send 'Keep alive' header
    /// - Parameter _timeout
    /// - Returns: self
    public func keepAlive(_send:Bool)->ConfigBuilder{
        self._keepAlive = _send
        return self
    }
    
    /// Set headers for api autologin requests
    /// - Parameter _headers: dictionary of headers
    /// - Returns: self
    public func headers(_headers:[String:String])->ConfigBuilder{
        self._headers = _headers
        return self
    }
    
    /// Set KeyChain group to store data in
    /// - Parameter _group: KeyChain access group
    /// - Returns: self
    public func keyChainGroup(_group:String)->ConfigBuilder{
        self._keyChainGroup = _group
        return self
    }
    
    /// Create instance of `Config`
    /// - Returns: new instance of `Config`
    public func build()->Config{
        return Config(url: _url,
                      clientId: _clientId,
                      clientSecret: _clientSecret,
                      headers: _headers,
                      keyChainGroup: _keyChainGroup,
                      connectTimeout: _connectTimeout,
                      readTimeout: _readTimeout,
                      retryCount: _retryCount,
                      keepAlive: _keepAlive)
    }
}

/**
 Entry class for sdk usage
*/

public class UIDM {
    public static let SDK_VERSION = "1.0.0"
    /// Preference key to check if autologin is blocked for SSO authentication
    public static let PREF_AUTOLOGIN_BLOCK = "roox.sdk.pref.autologin.block"
    /// Preference key to check if sdk analytics is enabled
    public static let PREF_ANALYTICS_ENABLED = "roox.sdk.pref.analytics.enabled"
    
    let config: Config
    let _sso: RooxAuthenticatorProtocol
    let _otp: RooxAuthenticatorProtocol
    let _api: RooxApiProtocol
    let _store: RooxStorageProtocol
    let _analytics: RooxAnalyticsProtocol
    internal init(config: Config,_api:RooxApiProtocol, _store: RooxStorageProtocol){
        self.config = config
        self._api = _api
        self._store = _store;
        self._sso = SSOAuthenticator(store:self._store, api: _api)
        self._otp = OTPAuthenticator(store:self._store, api: _api)
        self._analytics = RooxAnalytics(config: self.config,storage: self._store)
        
    }
    
    /// Factory method to create instance of sdk
    /// - Parameter config: `Config` to be used for this instance
    /// - Returns: new instance of class
    public static func create(config: Config)->UIDM{
        let store = RooxStore(config: config)
        let analytics = RooxAnalytics(config: config, storage: store)
        let api = RooxApi(config: config, network: DefaultNetwork(config: config), analytics: analytics)
        return UIDM(config: config,_api: api,_store: store)
    }
    
    internal static func create(config: Config, api:RooxApiProtocol, store:RooxStorageProtocol)->UIDM{
        
        return UIDM(config: config, _api: api, _store: store)
    }
    
    /// Check if there is locally stored token and its valid
    /// - Parameter success: block to call if token exists and is valid
    /// - Parameter failure: block to call if no token found `NoTokenError` or other `Error`
    public func getToken(success:@escaping (Token)->Void,failure:@escaping(Error)->Void){
        let token = self._store.load()
        if (token != nil){
            self._api.getAccessToken(formData: ["mpt":token!.token,"service":"dispatcher"], success: { (token:AccessTokenResponse) in
                self._store.store(token: token.storeToken())
                success(token.apiToken())
            }) { (error:Error) in
                if (error is AuthenticateError){
                    failure(NoTokenError())
                }else{
                    failure(error)
                }
            }
            
        }else{
            failure(NoTokenError())
        }
        
    }
    
    /// Remove locally stored token from storage
    /// - Parameter success: block to be called when token is removed
    public func logout(success: @escaping ()->Void){
        _store.remove()
        _store.storePref(key: UIDM.PREF_AUTOLOGIN_BLOCK, value: "true")
        success()
    }
    
    /// Get `RooxAuthenticatorProtocol` instance for SSO authentication
    /// - Returns: instance of `RooxAuthenticatorProtocol`
    public func sso()->RooxAuthenticatorProtocol{
        return _sso
    }
    
    /// Get `RooxAuthenticatorProtocol` instance for OTP authentication
    /// - Returns: instance of `RooxAuthenticatorProtocol`
    public func otp()->RooxAuthenticatorProtocol{
        return _otp
    }
    
    /// Get `RooxApiProtocol` instance for low level api calls
    /// - Returns: instance of `RooxApiProtocol`
    public func api()->RooxApiProtocol{
        return _api
    }
    
    /// Get `RooxStorageProtocol` instance to access to storage
    /// - Returns: instance of `RooxStorageProtocol`
    public func store()->RooxStorageProtocol{
        return _store
    }
    
    /// Control if analytics is sent by sdk
    /// - Parameter value: true if sdk can send its analytics events
    public func analyticsOptIn(value:Bool){
        var val = "NO"
        if (value){
            val = "YES"
        }
        self.store().storePref(key: UIDM.PREF_ANALYTICS_ENABLED, value: val)
    }
    
    /// Control if autologin is enabled
    /// - Parameter value: true if sdk can automatically login user
    public func autologinBlock(value:Bool){
        var val = "NO"
        if (value){
            val = "YES"
        }
        self.store().storePref(key: UIDM.PREF_AUTOLOGIN_BLOCK, value: val)
    }
    
    /// Check if analytics is sent by sdk
    /// - Returns: true if sdk can send its analytics events
    public func isAnalyticsOptIn() -> Bool{
        let val = self.store().loadPref(key: UIDM.PREF_ANALYTICS_ENABLED)
        if (val == "" || val == "YES"){
            return true
        }
        return false
    }
    
    /// Check if autologin is enabled
    /// - Returns: true if sdk can automatically login user
    public func isAutologinBlock() -> Bool{
        let val = self.store().loadPref(key: UIDM.PREF_AUTOLOGIN_BLOCK)
        if (val == "" || val == "YES"){
            return true
        }
        return false
    }
}
