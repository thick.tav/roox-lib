//
//  RooxError.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation

/// Base error class for internal sdk errors
public class RooxError: Error {
    
}

/// Unexpected server error
public class ServerError: Error{
    
}

/// No valid locally stored tokens found
public class NoTokenError:Error{
    
}

/// Error used by authenticators to request additional information for authentication
public class AuthenticateError:RooxError{
    /// information about missing fields or authentication errors
    public let authForm:AuthenticationResponse
    init(authForm:AuthenticationResponse){
        self.authForm = authForm
    }
}
