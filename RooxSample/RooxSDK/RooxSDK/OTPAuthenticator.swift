//
//  OTPAuthenticator.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import Foundation

internal class OTPAuthenticator: RooxAuthenticatorProtocol{
    let _api:RooxApiProtocol
    var lastAuthError: AuthenticateError?
    let store:RooxStorageProtocol
    init(store:RooxStorageProtocol,api:RooxApiProtocol){
        self._api = api
        self.store = store
    }
    func authenticate(formData: [String : String], success: @escaping (Token) -> Void, failure: @escaping (Error) -> Void) {
        self.authenticate(success: success, failure: failure)
    }
    
    func authenticate(success: @escaping (Token) -> Void, failure: @escaping (Error) -> Void) {
        self._api.getAccessToken(formData: ["service":"dispatcher"], success: { (token:AccessTokenResponse) in
            self.store.store(token: token.storeToken())
            success(token.apiToken())
        }) { (error:Error) in
            if (error is AuthenticateError){
                let aue = error as! AuthenticateError
                self._api.getAccessToken(formData: [
                    "_eventId":"login-by-otp",
                    "service":"dispatcher",
                    "execution":aue.authForm.execution
                    ], success: { (token:AccessTokenResponse) in
                        self.store.store(token: token.storeToken())
                        success(token.apiToken())
                        
                        
                }, failure: { (error:Error) in
                    if (error is AuthenticateError){
                        self.lastAuthError = error as? AuthenticateError
                    }
                    failure(error)
                })
            }
        }
    }
    
    func continueAuthenticate(formData: [String : String], success: @escaping (Token) -> Void, failure: @escaping (Error) -> Void) {
        if (lastAuthError != nil){
            let step = lastAuthError!.authForm.step
            var requestData = [String:String]()
            if ("enter_otp_form" == step){
                requestData = ["_eventId":"validate","service":"otp_operation_token","execution":lastAuthError!.authForm.execution]
            }else {
                requestData = ["_eventId":"next","service":"dispatcher","execution":lastAuthError!.authForm.execution]
            }
            self._api.getAccessToken(formData: formData.merging(requestData, uniquingKeysWith: { (k:String, _) -> String in
                k
            }), success: { (token:AccessTokenResponse) in
                self.store.store(token: token.storeToken())
                success(token.apiToken())
            }) { (error:Error) in
                if (error is AuthenticateError){
                    self.lastAuthError = error as? AuthenticateError
                }
                failure(error)
            }
        }
    }
    
}
