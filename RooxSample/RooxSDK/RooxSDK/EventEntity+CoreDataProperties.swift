//
//  EventEntity+CoreDataProperties.swift
//  RooxSDK
//
//  Created by Alexandr Timoshenko on 4/15/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//
//

import Foundation
import CoreData


extension EventEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EventEntity> {
        return NSFetchRequest<EventEntity>(entityName: "EventEntity")
    }

    @NSManaged public var eventName: String?

}
