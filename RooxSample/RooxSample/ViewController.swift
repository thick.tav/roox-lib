//
//  ViewController.swift
//  RooxSample
//
//  Created by Alexandr Timoshenko on 2/7/19.
//  Copyright © 2019 Alexandr Timoshenko. All rights reserved.
//

import UIKit
import RooxSDK
import CoreData
//import UIDM
class ViewController: UIViewController {
    var sdk:UIDM? = nil
    var auth:RooxAuthenticatorProtocol? = nil
    @IBOutlet weak var formStackView: UIStackView!
    @IBOutlet weak var formScrollView: UIScrollView!
    @IBOutlet weak var baseUrl: UITextField!
    @IBOutlet weak var clientId: UITextField!
    @IBOutlet weak var clientSecret: UITextField!
    @IBOutlet weak var headerXForwardedFor: UITextField!
    @IBOutlet weak var headerXNokiaMSISDN: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var userNameHolder: UIStackView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPass: UITextField!
    @IBOutlet weak var msisdnHolder: UIStackView!
    @IBOutlet weak var otpHolder: UIStackView!
    @IBOutlet weak var otpCode: UITextField!
    @IBOutlet weak var msisdnValue: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var loginActions: UIStackView!
    @IBOutlet weak var mptLabel: UILabel!
    @IBOutlet weak var mptValue: UITextField!
    @IBOutlet weak var retryCount: UITextField!
    @IBOutlet weak var connectTimeout: UITextField!
    @IBOutlet weak var readTimeout: UITextField!
    @IBOutlet weak var keepAlive: UISwitch!
    @IBOutlet weak var otpDetails: UILabel!
    @IBOutlet weak var analytics: UISwitch!
    @IBOutlet weak var accessTokenValue: UITextField!
    @IBOutlet weak var accessTokenCheckResult: UILabel!
    @IBOutlet weak var loginActionsHolder: UIStackView!
    @IBOutlet weak var blockAutologin: UISwitch!
    var lastError:AuthenticateError?=nil
    var alert: UIAlertController?
    func displayLoading(){
        DispatchQueue.main.async {
            self.alert = UIAlertController(title: "", message: "pls wait", preferredStyle: .alert)
            let progressBar = UIProgressView(progressViewStyle: .default)
            progressBar.setProgress(0.0, animated: true)
            progressBar.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
            self.alert!.view.addSubview(progressBar)
            self.present(self.alert!, animated: true, completion: nil)
        }
    }
    func hideLoading(){
        DispatchQueue.main.async {
            self.alert?.dismiss(animated: true, completion: {
                
            })
            self.alert = nil
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        baseUrl.text = UserDefaults.standard.string(forKey: "url") ?? "https://sso-mgf.demo.rooxteam.com"
        baseUrl.sizeToFit()
        clientId.text = UserDefaults.standard.string(forKey: "id") ?? "eshop"
        clientSecret.text = UserDefaults.standard.string(forKey: "secret") ?? "password"
        connectTimeout.text = UserDefaults.standard.string(forKey: "connectTimeout") ?? "30"
        readTimeout.text = UserDefaults.standard.string(forKey: "readTimeout") ?? "30"
        retryCount.text = UserDefaults.standard.string(forKey: "retryCount") ?? "3"
        keepAlive.isOn = UserDefaults.standard.bool(forKey: "keep-alive")
        loginActionsHolder.backgroundColor = UIColor.gray
        createSDK()
        analytics.isOn = sdk!.isAnalyticsOptIn()
        blockAutologin.isOn = sdk?.isAutologinBlock() ?? false
    
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        sdk?.getToken(success: { (token:Token) in
            self.displayUser(token: token)
        }, failure: { (error:Error) in
            self.displayNoUser()
        })
    }
    
    func createSDK(){
        
        sdk = UIDM.create(config: ConfigBuilder.create()
            .clientId(_id: clientId.text ?? "")
            .clientSecret(_secret: clientSecret.text ?? "")
            .url(_url:baseUrl.text ?? "")
            .headers(_headers: ["X-Nokia-MSISDN":headerXNokiaMSISDN.text ?? "","X-Forwarded-For":headerXForwardedFor.text ?? ""])
            .keyChainGroup(_group: "ru.roox.RooxSample")
            .build())
        UserDefaults.standard.set(baseUrl.text ?? "", forKey: "url")
        UserDefaults.standard.set(clientId.text ?? "", forKey: "id")
        UserDefaults.standard.set(clientSecret.text ?? "", forKey: "secret")
        UserDefaults.standard.set(retryCount.text ?? "3", forKey: "retryCount")
        UserDefaults.standard.set(connectTimeout.text ?? "30", forKey: "connectTimeout")
        UserDefaults.standard.set(readTimeout.text ?? "30", forKey: "readTimeout")
        UserDefaults.standard.set(keepAlive.isOn, forKey: "keep-alive")
    }
    
    func displayUser(token:Token){
        updateStoredMPT()
        resultLabel.text = String(format: "Has User: Token: %@", token.token)
        resultLabel.numberOfLines = 0
        resultLabel.sizeToFit()
        logoutBtn.isHidden = false
        loginActionsHolder.isHidden = true
        loginBtn.isHidden = true
        msisdnHolder.isHidden = true
        otpHolder.isHidden = true
        userNameHolder.isHidden = true
    
    }
    
    func displayNoUser(){
        updateStoredMPT()
        resultLabel.text = "No User"
        resultLabel.numberOfLines = 0
        resultLabel.sizeToFit()
        loginActionsHolder.isHidden = false
        logoutBtn.isHidden = true
        loginBtn.isHidden = true
        msisdnHolder.isHidden = true
        otpHolder.isHidden = true
        userNameHolder.isHidden = true
    }
    @IBAction func onInitClick(_ sender: Any) {
        createSDK()
    }
    
    @IBAction func onAutologinClick(_ sender: Any) {
        self.displayLoading()
        self.lastError = nil
        auth = sdk?.sso()
        auth?.authenticate(success: { (token:Token) in
            self.hideLoading()
            self.displayUser(token: token)
        }, failure: { (error:Error) in
            self.hideLoading()
            if (error is AuthenticateError){
                let ae = error as! AuthenticateError
                self.displayAuthError(error:ae)
            }else{
                self.resultLabel.text = String(format: "Fail: %@", error.localizedDescription)
                self.resultLabel.numberOfLines = 0
                self.resultLabel.sizeToFit()
            }
        })
    }
    
    @IBAction func onOTPClick(_ sender: Any) {
        self.lastError = nil
        self.auth = self.sdk?.otp()
        self.displayLoading()
        self.auth?.authenticate(success: { (token:Token) in
            self.hideLoading()
            self.displayUser(token: token)
        }, failure: { (error:Error) in
            self.hideLoading()
            if (error is AuthenticateError){
                self.lastError = error as? AuthenticateError
                self.displayAuthError(error: self.lastError!)
            }
        })
    }
    @IBAction func onUserPassClick(_ sender: Any) {
        self.lastError = nil
        self.auth = self.sdk?.sso()
        displayAuthForm()
    }
    
    @IBAction func onLoginClick(_ sender: Any) {
        var formData = [String:String]()
        let success = { (token:Token) in
            self.hideLoading()
            self.displayUser(token: token)
        }
        let failure = { (error:Error) in
            self.hideLoading()
            if (error is AuthenticateError){
                self.lastError = error as? AuthenticateError
                self.displayAuthError(error: self.lastError!)
            }
        }
        self.displayLoading()
        if (lastError != nil){
            let step = self.lastError!.authForm.step
            if ("auth_form" == step){
                formData = ["username":userName.text ?? "",
                            "password":userPass.text ?? ""]
            }else if ("enter_otp_form" == step){
                formData = ["otpCode":otpCode.text ?? ""]
            }else if ("login-by-otp-form" == step){
                formData = ["msisdn":msisdnValue.text ?? ""]
            }
            self.auth?.continueAuthenticate(formData: formData, success: success, failure: failure)
        }else{
            self.auth?.authenticate(formData: ["username":userName.text ?? "",
                                     "password":userPass.text ?? ""],success: success, failure: failure)
        }
        
    }
    @IBAction func onLogoutClick(_ sender: Any) {
        sdk?.logout {
            self.displayNoUser()
            self.blockAutologin.isOn = self.sdk?.isAutologinBlock() ?? false
        }
    }
    
    func displayAuthError(error:AuthenticateError){
        self.resultLabel.text =  error.authForm.form.errors.map { (error:FormError) -> String in
            error.message
        }.joined(separator: "\n")
        self.resultLabel.numberOfLines = 0
        self.resultLabel.sizeToFit()
        let step = error.authForm.step
        if ("auth_form" == step){
            displayAuthForm()
        }else if ("otp_form" == step){
            displayOtpForm(error:error)
        } else if ("enter_otp_form" == step){
            displayOtpForm(error:error)
        }else if ("login-by-otp-form" == step){
            displayMSISDN()
        }
    }
    func displayAuthForm(){
        loginBtn.isHidden = false
        userNameHolder.isHidden = false
        msisdnHolder.isHidden = true
        otpHolder.isHidden = true
    }
    
    func displayOtpForm(error: AuthenticateError){
        loginBtn.isHidden = false
        msisdnHolder.isHidden = true
        otpHolder.isHidden = false
        userNameHolder.isHidden = true
        otpDetails.text = String(format: "nextOtpCodePediod: %d\notpCodeAvailableAttempts: %d\nexpireOtpCodeTime: %d\nmsisdn: %@",
                                 error.authForm.view?.nextOtpCodePeriod ?? 0,
                                 error.authForm.view?.otpCodeAvailableAttempts ?? 0,
                                 error.authForm.view?.expireOtpCodeTime ?? 0,
                                 error.authForm.view?.msisdn ?? "")
        otpDetails.numberOfLines = 0
        otpDetails.sizeToFit()
    }
    
    func displayMSISDN(){
        otpHolder.isHidden = true
        loginBtn.isHidden = false
        msisdnHolder.isHidden = false
        userNameHolder.isHidden = true
    }
    
    @IBAction func setMptTokenClick(_ sender: Any) {
        sdk?.store().store(token: Token.create(token: mptValue.text ?? ""))
        updateStoredMPT()
    }
    @IBAction func refreshTokenClick(_ sender: Any) {
        self.displayLoading()
        sdk?.getToken(success: { (token:Token) in
            self.hideLoading()
            self.displayUser(token: token)
        }, failure: { (error:Error) in
            self.hideLoading()
            self.displayNoUser()
        })
    }
    
    func updateStoredMPT(){
        mptLabel.text = String(format: "MPT token: %@", sdk?.store().load()?.token ?? "")
        mptLabel.numberOfLines = 0
        mptLabel.sizeToFit()
    }
    @IBAction func keepAliveChanged(_ sender: Any) {
        UserDefaults.standard.set(keepAlive.isOn, forKey: "keep-alive")
    }
    @IBAction func analyticsChanged(_ sender: Any) {
        sdk?.analyticsOptIn(value: analytics.isOn)
    }
    @IBAction func accessTokenCheckClick(_ sender: Any) {
        self.displayLoading()
        sdk?.api().getTokenInfo(token: accessTokenValue.text ?? "", success: { (info:AccessTokenInfo) in
            self.hideLoading()
            self.accessTokenCheckResult.text = "ok \(info.description)"
            self.accessTokenCheckResult.numberOfLines = 0
            self.accessTokenCheckResult.sizeToFit()
        }, failure: { (error:Error) in
            self.hideLoading()
            self.accessTokenCheckResult.text = "error \(error.localizedDescription)"
            self.accessTokenCheckResult.numberOfLines = 0
            self.accessTokenCheckResult.sizeToFit()
        })
    }
    @IBAction func autolockBlockChange(_ sender: Any) {
        sdk?.autologinBlock(value: self.blockAutologin.isOn)
    }
    @IBAction func tokenInfoClick(_ sender: Any) {
        self.displayLoading()
        self.sdk?.getToken(success: { (token:Token) in
            self.sdk?.api().getTokenInfo(token: token.token, success: { (info:AccessTokenInfo) in
                self.hideLoading()
                self.accessTokenCheckResult.text = "ok \(info.description)"
                self.accessTokenCheckResult.numberOfLines = 0
                self.accessTokenCheckResult.sizeToFit()
                
            }, failure: { (error:Error) in
                self.hideLoading()
                self.accessTokenCheckResult.text = "error \(error.localizedDescription)"
                self.accessTokenCheckResult.numberOfLines = 0
                self.accessTokenCheckResult.sizeToFit()
            })
        }, failure: { (error:Error) in
            self.hideLoading()
            self.accessTokenCheckResult.text = "error \(error.localizedDescription)"
            self.accessTokenCheckResult.numberOfLines = 0
            self.accessTokenCheckResult.sizeToFit()
        })
    }
    @IBAction func getAccessToken(_ sender: Any) {
        self.displayLoading()
        self.sdk?.getToken(success: { (token:Token) in
            self.hideLoading()
            self.accessTokenCheckResult.text = "ok \(token.token)"
            self.accessTokenCheckResult.numberOfLines = 0
            self.accessTokenCheckResult.sizeToFit()
        }, failure: { (error:Error) in
            self.hideLoading()
            self.accessTokenCheckResult.text = "error \(error.localizedDescription)"
            self.accessTokenCheckResult.numberOfLines = 0
            self.accessTokenCheckResult.sizeToFit()
        })
    }
}

