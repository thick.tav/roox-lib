//
//  ViewController.swift
//  App A
//
//  Created by Alexey Kuznetsov on 27/12/2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit
import Alamofire
import KeychainSwift

class ViewController: UIViewController {

	@IBOutlet weak var labelTitlw: UILabel!
	@IBOutlet weak var tfPass: UITextField!
	@IBOutlet weak var tfLogin: UITextField!
	let keychain = KeychainSwift()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		keychain.accessGroup = "TV932C4G5J.Roox.Shared" // Use your own access goup
		//9876543210
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.labelTitlw.text = ""
		if let token = keychain.get("AccessToken") {
			Roox.validateToken(token: token, completion: { (text) in
				if let text = text {
					self.labelTitlw.text = text
					self.showMessage(text: "Успешный логин!")
				} else {
					self.keychain.delete("AccessToken")
					self.showMessage(text: "Не можем авторизоваться. Неверный токен. Попробуйте еще раз")
				}
			})
		}
	}
	
	@IBAction func loginTap(_ sender: Any) {
		let clientSecret = "password"
		
		Roox.getExecution(clientSecret: clientSecret) { (result, execution) in
			if let execution = execution {
				print(execution)
				Roox.getToken(clientSecret: clientSecret, login: self.tfLogin.text ?? "", password: self.tfPass.text ?? "", execution: execution, completion: { (result, accessToken, refreshToken) in
					if let accessToken = accessToken {
						print(accessToken)
						self.keychain.set(accessToken, forKey: "AccessToken")
						Roox.validateToken(token: accessToken, completion: { (text) in
							if let text = text {
								self.labelTitlw.text = text
								self.showMessage(text: "Успешный логин!")
							} else {
								self.showMessage(text: "Не можем авторизоваться. Попробуйте еще раз")
							}
						})
					} else {
						self.showMessage(text: "Не можем авторизоваться. Попробуйте еще раз")
					}
				})
			} else {
				self.showMessage(text: "Не можем авторизоваться. Попробуйте еще раз")
			}
		}
	}
	
	func showMessage(text: String) {
		let alert = UIAlertController(title: "", message: text, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(alert, animated: true, completion: nil)
	}
	
}

class Roox {
	static func getExecution(clientSecret: String, completion: @escaping (Bool, String?) -> Void) {
		/**
		Request
		post https://sso-mgf.demo.rooxteam.com/sso/oauth2/access_token
		*/
		
		// Add Headers
		let headers = [
			"Connection":"keep-alive",
			"Accept-Encoding":"gzip, deflate",
			"Accept":"application/json",
			"User-Agent":"roox-autotests",
			"Content-Type":"application/x-www-form-urlencoded",
			"Content-Length":"131",
			"X-Forwarded-For":"5.134.221.198",
			]
		
		// Form URL-Encoded Body
		let body = [
			"client_secret":clientSecret,
			"grant_type":"urn:roox:params:oauth:grant-type:m2m",
			"realm":"/customer",
			"service":"dispatcher",
			"client_id":"mlk",
			]
		
		// Fetch Request
		Alamofire.request("https://sso-mgf.demo.rooxteam.com/sso/oauth2/access_token", method: .post, parameters: body, encoding: URLEncoding.default, headers: headers)
			.validate(statusCode: 200..<300)
			.responseJSON { response in
				if let json = response.result.value as? [String: Any],
					let execution = json["execution"] as? String {
					completion(true, execution)
				} else {
					debugPrint("HTTP Request failed: \(String(describing: response.result.error))")
					completion(false, nil)
				}
		}
	}
	
	static func getToken(clientSecret: String, login: String, password: String, execution: String, completion: @escaping (Bool, String?, String?) -> Void) {
		/**
		Request
		post https://sso-mgf.demo.rooxteam.com/sso/oauth2/access_token
		*/
		
		// Add Headers
		let headers = [
			"Connection":"keep-alive",
			"Accept-Encoding":"gzip, deflate",
			"Accept":"application/json",
			"User-Agent":"roox-autotests",
			"Content-Type":"application/x-www-form-urlencoded",
			"Content-Length":"131",
			"X-Forwarded-For":"5.134.221.198",
			]
		
		// Form URL-Encoded Body
		let body = [
			"client_secret": clientSecret,
			"grant_type":"urn:roox:params:oauth:grant-type:m2m",
			"realm":"/customer",
			"service":"dispatcher",
			"client_id":"mlk",
			"execution":execution,
			"username": login,
			"password": password,
			"_eventId":"next",
			]
		
		// Fetch Request
		Alamofire.request("https://sso-mgf.demo.rooxteam.com/sso/oauth2/access_token", method: .post, parameters: body, encoding: URLEncoding.default, headers: headers)
			.validate(statusCode: 200..<300)
			.responseJSON { response in
				if let json = response.result.value as? [String: Any],
					let accessToken = json["access_token"] as? String,
					let refreshToken = json["refresh_token"] as? String {
					completion(true, accessToken, refreshToken)
				} else {
					debugPrint("HTTP Request failed: \(String(describing: response.result.error))")
					completion(false, nil, nil)
				}
		}
	}
	
	static func validateToken(token: String, completion: @escaping (String?) -> Void) {
		/**
		Request
		post https://sso-mgf.demo.rooxteam.com/sso/oauth2/tokeninfo
		*/
		
		// Add Headers
		let headers = [
			"Connection":"keep-alive",
			"Accept-Encoding":"gzip, deflate",
			"Accept":"application/json",
			"User-Agent":"roox-autotests",
			"Host":"sso-mgf.demo.rooxteam.com",
			"Content-Type":"application/json",
			]

		
		// Fetch Request
		Alamofire.request("https://sso-mgf.demo.rooxteam.com/sso/oauth2/tokeninfo?access_token=\(token)", method: .post, parameters: [:], encoding: JSONEncoding.default, headers: headers)
			.validate(statusCode: 200..<300)
			.responseJSON { response in
				if let json = response.result.value as? [String: Any],
					let token = json["access_token"] as? String,
					let login = json["cn"] as? String,
					let expires = json["expires_in"] as? Int {
					completion("токен: \(token) \n  истекает через: \(expires) \n логин: \(login)")
				} else {
					debugPrint("HTTP Request failed: \(String(describing: response.result.error))")
					completion(nil)
				}
		}
	}
	
	

	
}

